#ifndef _VEML6070H
#define _VEML6070H
/*!
 * @file Adafruit_VEML6070.h
 *
 * Designed specifically to work with the VEML6070 sensor from Adafruit
 * ----> https://www.adafruit.com/products/2899
 *
 * These sensors use I2C to communicate, 2 pins (SCL+SDA) are required
 * to interface with the breakout.
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.  
 *
 * Rewritten by semenenko_myk@ukr.net for stm32f103c8tx board configured with C.
 *
 * BSD license, all text here must be included in any redistribution.
 *
 */


// really unusual way of getting data, your read from two different addrs!

#define VEML6070_ADDR_H     (0x39) ///< High address
#define VEML6070_ADDR_L     (0x38) ///< Low address
#define VEML6070_ADDR_ARA   (0x0C) ///< Alert Resp Address (read to clear condition)


typedef enum veml6070_integrationtime {
  VEML6070_HALF_T,
  VEML6070_1_T,
  VEML6070_2_T,
  VEML6070_4_T,
} veml6070_integrationtime_t;

typedef union {
	  
    struct {
      uint8_t SD:1;
      uint8_t :1;
      uint8_t IT:2;
      uint8_t ACK_THD:1;
      uint8_t ACK:1;
    } bit;
	
    uint8_t reg;

} commandRegister;
  

  void _VEML6070_start(void);
  void VEML6070_begin(veml6070_integrationtime_t itime, TwoWire *twoWire = &Wire);
  void VEML6070_setInterrupt(bool state, bool level = 0);
  bool VEML6070_clearAck();
  uint16_t VEML6070_readUV(void);
  void VEML6070_waitForNext(void);
  void VEML6070_sleep(bool state);
  void _VEML6070_writeCommand();
  
#endif